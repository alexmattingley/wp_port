<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// // ** MySQL settings - You can get this info from your web host ** //
// /** The name of the database for WordPress */
// define('DB_NAME', 'alexmattingleydb_local');

// /** MySQL database username */
// define('DB_USER', 'root');

// /** MySQL database password */
// define('DB_PASSWORD', 'sandiego23');

// /** MySQL hostname */
// define('DB_HOST', 'localhost');

// /** Database Charset to use in creating database tables. */
// define('DB_CHARSET', 'utf8');

// /** The Database Collate type. Don't change this if in doubt. */
// define('DB_COLLATE', '');


$hostname = $_SERVER['HTTP_HOST']; //dev.madebygrizzly.com | localhost | Live server | etc..
// echo $hostname;
// exit;
switch ($hostname) {

	case 'alexmattingley.com': //this is our dev server		
		define('DB_NAME', 'alexmatt_dbremote');
		define('DB_USER', 'alexmatt_user');
		define('DB_PASSWORD', '0mwO03qlB3');
		define('DB_HOST', 'localhost');
		break;
	
	case 'alexmattingley.dev': //this is our dev server	
		define('DB_NAME', 'alexmattingleydb_local');
		define('DB_USER', 'root');
		define('DB_PASSWORD', 'sandiego23');
		define('DB_HOST', 'localhost');
		break;
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S8>ZofN&:f(V+-G)|}P/>H.>(glQR4I/QjM|@+2J_>og@Pk/l8 W3(4besJ}vF<O');
define('SECURE_AUTH_KEY',  'm PQjehKt1~>R2pZ$k?QHK{z ;)_v~b+21]~TLc:~]?&`cW.|MLBLyJ(>Ly7Xhdo');
define('LOGGED_IN_KEY',    'Y+(#CE|kE0e+d:^G|#J*Y{|=6{|aA E`]J{>_zWbgv`|y^`#jHK?FamOs|NUEVhA');
define('NONCE_KEY',        '6X|Gn+jA$wHBAhjw=@182?*@DtI)E02FnG,J;:8+(+GAt6l_GzKU&3||C[q`(+]3');
define('AUTH_SALT',        'dC/dYT`4?eEE|NO<)hDc8ml(K+&L|!J|)oT-!^9mj@Sn!y51.@+`PM Yw|*h|-gL');
define('SECURE_AUTH_SALT', 'asQGtAq8!+[1K83^4iFal}YKIiKHx){1_89R`IO+85qUbZ:G0)Q(To d5RP0L9P6');
define('LOGGED_IN_SALT',   '1 gl_>gMZb[[5e$^?YcQIF9-mBZ|C$+_EqK{jId.}C<m0eLgDNBES-`VFEzhWS[7');
define('NONCE_SALT',       'k5U+)SXi6Y$<||Nz7~d9wH[oF#z#^c_2+VNrfUxQZ;nHr^2qR+6i?0EOg;X](u|[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

if(is_admin()) {
	add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
	define( 'FS_CHMOD_DIR', 0751 );
}
