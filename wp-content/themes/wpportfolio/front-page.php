<!-- testing -->
<?php get_header(); ?>
	<div class="container clearfix">
		<?php 
		$args = array (
			'post_type' => 'post',
			'category_name' => 'featured',
			'posts_per_page' => 1
		);
		$the_query = new WP_Query( $args );

		?>
		<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="grid_12 omega">
			<h5>Featured Post</h5>
		</div>
		<div class="push_2 grid_10 omega clearfix">
			<article>
					<?php get_template_part('content', 'post') ?>
			</article>
		</div>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="grid_12 omega clearfix">
		<div class="grid_6">
			<article>
				<h5>Recent Post</h5>
				<?php 
				$args = array (
					'post_type' => 'post',
					'cat' => -5, //this is excluding category featured. Find this by going to wp backend, going to the category and checking the URL The ID in the Url is the correct value.
					'posts_per_page' => 1
				);
				$the_query = new WP_Query( $args );

				?>
				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				
				<?php get_template_part('content', 'post') ?>
				
				<?php endwhile; ?>
				<?php endif; ?>

			</article>
		</div>
		
			<?php

				$args = array(
					'post_type' => 'work',
					'posts_per_page' => 1
				);

				$the_query = new WP_Query( $args );

			?>


			<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<?php get_template_part('content', 'work') ?>

			<?php endwhile; ?>

			<?php endif; ?>					
	</div>


			<?php get_template_part('content', 'testimonials') ?>

<?php get_footer(); ?>

